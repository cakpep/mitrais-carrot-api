package com.mitrais.carrot.config.basicauth;

import com.mitrais.carrot.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.io.PrintWriter;
import javax.naming.AuthenticationException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * use this configuration if we want to using basic auth as web security app
 */
@Component
public class BasicAuthenticationPoint extends BasicAuthenticationEntryPoint {

    private static final Logger logMe = LoggerFactory.getLogger(BasicAuthenticationPoint.class);

    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authEx) throws IOException, ServletException {
        response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        logMe.error("Responding with unauthorized error. Message - {}", authEx.getMessage());
        PrintWriter writer = response.getWriter();
        writer.println("HTTP Status 401 - " + authEx.getMessage());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setRealmName(Config.REALM_NAME);
        super.afterPropertiesSet();
    }

}
