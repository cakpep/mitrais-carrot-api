package com.mitrais.carrot.config;

import com.mitrais.carrot.config.basicauth.BasicAuthenticationPoint;
import com.mitrais.carrot.config.jwt.CustomUserDetailsService;
import com.mitrais.carrot.config.jwt.JwtAuthenticationEntryPoint;
import com.mitrais.carrot.config.jwt.JwtAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    CustomUserDetailsService customUserDetailsService;

    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter();
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        if (Config.SECURE_MY_APP_WITH_JWT_SECURITY) {
            this.configureAuthenticationManagerBuilderJWT(authenticationManagerBuilder);
        } else {
            this.configureAuthenticationManagerBuilderBasicAuth(authenticationManagerBuilder);
        }
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        if (Config.SECURE_MY_APP_WITH_JWT_SECURITY) {
            this.configureHttpSecurityJWT(http);
        } else {
            this.configureHttpSecurityBasicAuth(http);
        }
    }

    /**
     * this config will use when JWT is use
     *
     * @param authenticationManagerBuilder
     * @throws Exception
     */
    private void configureAuthenticationManagerBuilderJWT(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(customUserDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    /**
     * secure app using basic auth
     *
     * @param http
     * @throws Exception
     */
    private void configureHttpSecurityJWT(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedHandler)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/",
                        "/favicon.ico",
                        "/**/*.png",
                        "/**/*.gif",
                        "/**/*.svg",
                        "/**/*.jpg",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js")
                .permitAll()
                .antMatchers(
                        Config.BASE_URI_API + "/signin",
                        Config.BASE_URI_API + "/signup",
                        Config.BASE_URI_API + "/users/availability",
                        "/swagger-ui.html",
                        "/v2/api-docs"
                )
                .permitAll()
                .anyRequest()
                .authenticated();

        // Add our custom JWT security filter
        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    /**
     * this config will use when basic auth is use
     *
     * @param authenticationManagerBuilder
     * @throws Exception
     */
    private void configureAuthenticationManagerBuilderBasicAuth(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.inMemoryAuthentication()
                .withUser("cakpep").password("{noop}dev").roles("USER")
                .and()
                .withUser("prod").password("{noop}live").roles("MANAGER");
    }

    /**
     * secure app using basic auth
     *
     * @param http
     * @throws Exception
     */
    private void configureHttpSecurityBasicAuth(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .realmName(Config.REALM_NAME)
                .authenticationEntryPoint(new BasicAuthenticationPoint());
    }
}
