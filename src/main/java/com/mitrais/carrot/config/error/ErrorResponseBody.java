package com.mitrais.carrot.config.error;

import java.util.Date;

public class ErrorResponseBody {

    private Date timestamp;
    private String details;
    private String message;

    public ErrorResponseBody(Date timestamp, String message, String details) {
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }
}
