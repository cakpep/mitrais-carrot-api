package com.mitrais.carrot.config;

/**
 * main app config
 *
 */
public class Config {

    /**
     * base api url
     */
    public static final String BASE_URI_API = "/api/v1";

    /**
     * use in web mvc @configuration
     */
    public static final long MAX_AGE_SECS = 3600;

    /**
     * if true the security will use JWT if false the security will using Basic
     * Auth
     */
    public static final Boolean SECURE_MY_APP_WITH_JWT_SECURITY = true;

    /**
     * realm name on basic auth
     */
    public static final String REALM_NAME = "Mitrais Carrot Rest API App";

    /**
     * base uri login api for jwt
     */
    public static final String JWT_LOGIN_URI = Config.BASE_URI_API + "/auth";
    public static final String JWT_SECRET = "carrot";
    public static final String JWT_TOKEN_PREFIX = "Bearer";
    public static final String JWT_HEADER_STRING = "Authorization";
    public static final long JWT_EXPIRATION_TIME = 864_000_000; // 10 days
}
