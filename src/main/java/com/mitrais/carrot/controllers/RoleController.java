package com.mitrais.carrot.controllers;

import com.mitrais.carrot.config.Config;
import com.mitrais.carrot.models.Role;
import com.mitrais.carrot.payload.ApiResponse;
import com.mitrais.carrot.repositories.RoleRepository;
import com.mitrais.carrot.validation.exception.ResourceNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(Config.BASE_URI_API)
public class RoleController {

    @Autowired
    public RoleRepository roleRepository;

    /**
     * get all data
     *
     * @return
     */
    @GetMapping("/roles")
    @ResponseBody
    public Iterable<Role> all() {
        return roleRepository.findAllBydeletedIsNull();
    }

    /**
     * create new data
     *
     * @param body
     * @return
     */
    @PostMapping("/roles")
    @ResponseBody
    public Role save(@Valid @RequestBody Role body) {
        return roleRepository.save(body);
    }

    /**
     * get detail by id
     *
     * @param id
     * @return
     */
    @GetMapping("/roles/{id}")
    @ResponseBody
    public Role detail(@PathVariable Integer id) {
        return roleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
    }

    /**
     * update data
     *
     * @param id
     * @param body
     * @return
     */
    @PutMapping("/roles/{id}")
    @ResponseBody
    public Role update(@PathVariable Integer id, @Valid @RequestBody Role body) {
        Role model = roleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
//        BeanUtils.copyProperties(body, role);
//        role.setId(body.getId());
        model.setRoleName(body.getRoleName());
        return roleRepository.save(model);
    }

    /**
     * action delete to set is deleted = 1
     *
     * @param id
     * @return
     */
    @DeleteMapping("/roles/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        Role sl = roleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        sl.setDeleted(true);
        roleRepository.save(sl);
        return new ResponseEntity(
                new ApiResponse(true, "Data id : " + id + "deleted successfully"),
                HttpStatus.OK
        );
    }
}
