package com.mitrais.carrot.controllers;

import com.mitrais.carrot.config.Config;
import com.mitrais.carrot.models.SharingLevel;
import com.mitrais.carrot.payload.ApiResponse;
import com.mitrais.carrot.repositories.SharingLevelRepository;
import com.mitrais.carrot.validation.exception.ResourceNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping(Config.BASE_URI_API)
public class SharingLevelController {

    @Autowired
    public SharingLevelRepository sharingLevelRepository;

    /**
     * get all data
     *
     * @return
     */
    @GetMapping("/sharing-levels")
    @ResponseBody
    public Iterable<SharingLevel> all() {
        return sharingLevelRepository.findAllBydeletedIsNull();
    }

    /**
     * create new data
     *
     * @param body
     * @return
     */
    @PostMapping("/sharing-levels")
    @ResponseBody
    public SharingLevel save(@Valid @RequestBody SharingLevel body) {
        return sharingLevelRepository.save(body);
    }

    /**
     * get detail by id
     *
     * @param id
     * @return
     */
    @GetMapping("/sharing-levels/{id}")
    @ResponseBody
    public SharingLevel show(@PathVariable Integer id) {
        return sharingLevelRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
    }

    /**
     * update data
     *
     * @param id
     * @param body
     * @return
     */
    @PutMapping("/sharing-levels/{id}")
    @ResponseBody
    public SharingLevel update(@PathVariable Integer id, @Valid @RequestBody SharingLevel body) {
        SharingLevel shareLevel = sharingLevelRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        BeanUtils.copyProperties(body, shareLevel);
        shareLevel.setId(id);
        return sharingLevelRepository.save(shareLevel);
    }

    /**
     * action delete to set is deleted = 1
     *
     * @param id
     * @return
     */
    @DeleteMapping("/sharing-levels/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        SharingLevel sl = sharingLevelRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        sl.setDeleted(true);
        sharingLevelRepository.save(sl);
        return new ResponseEntity(
                new ApiResponse(true, "Data is Deleted!"),
                HttpStatus.OK
        );
    }
}
