package com.mitrais.carrot.controllers;

import com.mitrais.carrot.config.Config;
import com.mitrais.carrot.models.BarnSetting;
import com.mitrais.carrot.payload.ApiResponse;
import com.mitrais.carrot.repositories.BarnSettingRepository;
import com.mitrais.carrot.validation.exception.ResourceNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(Config.BASE_URI_API)
public class BarnSettingController {

    @Autowired
    public BarnSettingRepository barnSettingRepository;

    /**
     * get all data
     *
     * @return
     */
    @GetMapping("/barns-settings")
    @ResponseBody
    public Iterable<BarnSetting> all() {
        return barnSettingRepository.findAllBydeletedIsNull();
    }

    /**
     * create new data
     *
     * @param body
     * @return
     */
    @PostMapping("/barns-settings")
    @ResponseBody
    public BarnSetting save(@Valid @RequestBody BarnSetting body) {
        return barnSettingRepository.save(body);
    }

    /**
     * get detail by id
     *
     * @param id
     * @return
     */
    @GetMapping("/barns-settings/{id}")
    @ResponseBody
    public BarnSetting detail(@PathVariable Integer id) {
        return barnSettingRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
    }

    /**
     * update data
     *
     * @param id
     * @param body
     * @return
     */
    @PutMapping("/barns-settings/{id}")
    @ResponseBody
    public BarnSetting update(@PathVariable Integer id, @Valid @RequestBody BarnSetting body) {
        BarnSetting model = barnSettingRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        BeanUtils.copyProperties(body, model);
        model.setId(id);
        return barnSettingRepository.save(model);
    }

    /**
     * action delete to set is deleted = 1
     *
     * @param id
     * @return
     */
    @DeleteMapping("/barns-settings/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        BarnSetting sl = barnSettingRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        sl.setDeleted(true);
        barnSettingRepository.save(sl);
//        barnSettingRepository.delete(sl.get());
        return new ResponseEntity(
                new ApiResponse(true, "Data id : " + id + "deleted successfully"),
                HttpStatus.OK
        );
    }
}
