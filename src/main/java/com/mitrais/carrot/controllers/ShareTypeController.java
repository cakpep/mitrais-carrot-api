package com.mitrais.carrot.controllers;

import com.mitrais.carrot.config.Config;
import com.mitrais.carrot.models.ShareType;
import com.mitrais.carrot.payload.ApiResponse;
import com.mitrais.carrot.repositories.ShareTypeRepository;
import com.mitrais.carrot.validation.exception.ResourceNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(Config.BASE_URI_API)
public class ShareTypeController {

    @Autowired
    public ShareTypeRepository shareTypeRepository;

    /**
     * get all data
     * @return 
     */
    @GetMapping("/sharing-types")
    @ResponseBody
    public Iterable<ShareType> all() {
        return shareTypeRepository.findAllBydeletedIsNull();
    }

    /**
     * create new data
     * @param body
     * @return 
     */
    @PostMapping("/sharing-types")
    @ResponseBody
    public ShareType save(@Valid @RequestBody ShareType body) {
        return shareTypeRepository.save(body);
    }

    /**
     * get detail by id
     * @param id
     * @return 
     */
    @GetMapping("/sharing-types/{id}")
    @ResponseBody
    public ShareType detail(@PathVariable Integer id) {
        return shareTypeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
    }

    /**
     * update data
     * @param id
     * @param body
     * @return 
     */
    @PutMapping("/sharing-types/{id}")
    @ResponseBody
    public ShareType update(@PathVariable Integer id, @Valid @RequestBody ShareType body) {
        ShareType model = shareTypeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        BeanUtils.copyProperties(body, model);
        model.setId(id);
        return shareTypeRepository.save(model);
    }

    /**
     * action delete to set is deleted = 1
     * @param id
     * @return 
     */
    @DeleteMapping("/sharing-types/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        ShareType sl = shareTypeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        sl.setDeleted(true);
        shareTypeRepository.save(sl);
        return new ResponseEntity(
                new ApiResponse(true, "Data id : " + id + "deleted successfully"),
                HttpStatus.OK
        );
    }
}
