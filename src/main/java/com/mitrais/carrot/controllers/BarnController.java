package com.mitrais.carrot.controllers;

import com.mitrais.carrot.config.Config;
import com.mitrais.carrot.models.Barn;
import com.mitrais.carrot.payload.ApiResponse;
import com.mitrais.carrot.repositories.BarnRepository;
import com.mitrais.carrot.validation.exception.ResourceNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(Config.BASE_URI_API)
public class BarnController {

    @Autowired
    public BarnRepository barnRepository;

    /**
     * get all data
     * @return 
     */
    @GetMapping("/barns")
    @ResponseBody
    public Iterable<Barn> all() {
        return barnRepository.findAllBydeletedIsNull();
    }

    /**
     * create new data
     * @param body
     * @return 
     */
    @PostMapping("/barns")
    @ResponseBody
    public Barn save(@Valid @RequestBody Barn body) {
        return barnRepository.save(body);
    }

    /**
     * get data by id
     * @param id
     * @return 
     */
    @GetMapping("/barns/{id}")
    @ResponseBody
    public Barn detail(@PathVariable Integer id) {
        return barnRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
    }

    /**
     * update data by id
     * @param id
     * @param body
     * @return 
     */
    @PutMapping("/barns/{id}")
    @ResponseBody
    public Barn update(@PathVariable Integer id, @Valid @RequestBody Barn body) {
        Barn model = barnRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        BeanUtils.copyProperties(model, body);
        model.setId(id);
        return barnRepository.save(model);
    }

    /**
     * delete data by id
     * @param id
     * @return 
     */
    @DeleteMapping("/barns/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        Barn sl = barnRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        sl.setDeleted(true);
        barnRepository.save(sl);
//        barnRepository.delete(sl.get());
        return new ResponseEntity(
                new ApiResponse(true, "Data id : " + id + "deleted successfully"),
                HttpStatus.OK
        );
    }
}
