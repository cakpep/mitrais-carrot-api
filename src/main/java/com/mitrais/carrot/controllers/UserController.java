package com.mitrais.carrot.controllers;

import com.mitrais.carrot.config.Config;
import com.mitrais.carrot.config.jwt.CurrentUser;
import com.mitrais.carrot.config.jwt.UserPrincipal;
import com.mitrais.carrot.models.User;
import com.mitrais.carrot.payload.ApiResponse;
import com.mitrais.carrot.payload.UserIdentityAvailability;
import com.mitrais.carrot.payload.UserProfile;
import com.mitrais.carrot.payload.UserSummary;
import com.mitrais.carrot.repositories.UserRepository;
import com.mitrais.carrot.validation.exception.ResourceNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping(Config.BASE_URI_API)
public class UserController {

    @Autowired
    private UserRepository userRepository;

    /**
     * get all data
     *
     * @return all user
     */
    @GetMapping("/users")
    @ResponseBody
    public Iterable<User> all() {
        return userRepository.findAllBydeletedIsNull();
    }

    /**
     * create new data
     *
     * @param body
     * @return User
     */
    @PostMapping("/users")
    @ResponseBody
    public User save(@Valid @RequestBody User body) {
        return userRepository.save(body);
    }

    /**
     * get detail by id
     *
     * @param id
     * @return
     */
    @GetMapping("/users/{id}")
    @ResponseBody
    public User show(@PathVariable Long id) {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
    }

    /**
     * update data
     *
     * @param id
     * @param body
     * @return
     */
    @PutMapping("/users/{id}")
    @ResponseBody
    public User update(@PathVariable Long id, @Valid @RequestBody User body) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        BeanUtils.copyProperties(body, user);
        user.setId(id);
        return userRepository.save(user);
    }

    /**
     * action delete to set is deleted = 1
     *
     * @param id
     * @return
     */
    @DeleteMapping("/users/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable Long id) {
        User sl = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        sl.setDeleted(true);
        userRepository.save(sl);
        return new ResponseEntity(
                new ApiResponse(true, "Data id : " + id + "deleted successfully"),
                HttpStatus.OK
        );
    }

    /**
     * get current user
     *
     * @param currentUser
     * @return UserSummary
     */
    @GetMapping("/users/me")
//    @PreAuthorize("hasRole('USER')")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getName());
        return userSummary;
    }

    /**
     * get user profile with more detailed data
     * @param currentUser
     * @return  UserProfile
     */
    @GetMapping("/users/myprofile")
    public UserProfile getUserProfile(@CurrentUser UserPrincipal currentUser) {
        User user = userRepository.findByUserName(currentUser.getUsername());
        if (user == null) {
            throw new ResourceNotFoundException("User", "username", currentUser.getUsername());
        }
        return new UserProfile(
                user.getId(),
                user.getUserName(),
                user.getEmail(),
                user.getName(),
                user.getCreatedTime()
        );
    }

    /**
     * check user availabilities by key and value available : username and email
     *
     * @param key
     * @param value
     * @return UserIdentityAvailability
     */
    @GetMapping("/users/availability")
    public UserIdentityAvailability checkUserAvailability(
            @RequestParam(value = "key") String key,
            @RequestParam(value = "value") String value
    ) {

        Boolean isAvailable = false;
        if ("username".equals(key)) {
            isAvailable = !userRepository.existsByUserName(value);
        } else if ("email".equals(key)) {
            isAvailable = !userRepository.existsByEmail(value);
        }
        return new UserIdentityAvailability(isAvailable);
    }
}
