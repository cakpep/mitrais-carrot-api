package com.mitrais.carrot.controllers;

import com.mitrais.carrot.config.Config;
import com.mitrais.carrot.models.Rewards;
import com.mitrais.carrot.payload.ApiResponse;
import com.mitrais.carrot.repositories.RewardsRepository;
import com.mitrais.carrot.validation.exception.ResourceNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(Config.BASE_URI_API)
public class RewardsController {

    @Autowired
    public RewardsRepository rewardsRepository;

    /**
     * get all data
     *
     * @return
     */
    @GetMapping("/rewards")
    @ResponseBody
    public Iterable<Rewards> all() {
        return rewardsRepository.findAllBydeletedIsNull();
    }

    /**
     * create new data
     *
     * @param body
     * @return
     */
    @PostMapping("/rewards")
    @ResponseBody
    public Rewards save(@Valid @RequestBody Rewards body) {
        return rewardsRepository.save(body);
    }

    /**
     * get detail by id
     *
     * @param id
     * @return
     */
    @GetMapping("/rewards/{id}")
    @ResponseBody
    public Rewards detail(@PathVariable Integer id) {
        return rewardsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
    }

    /**
     * update data
     *
     * @param id
     * @param body
     * @return
     */
    @PutMapping("/rewards/{id}")
    @ResponseBody
    public Rewards update(@PathVariable Integer id, @Valid @RequestBody Rewards body) {
        Rewards model = rewardsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        BeanUtils.copyProperties(body, model);
        model.setId(id);
        return rewardsRepository.save(model);
    }

    /**
     * action delete to set is deleted = 1
     *
     * @param id
     * @return
     */
    @DeleteMapping("/rewards/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        Rewards sl = rewardsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        sl.setDeleted(true);
        rewardsRepository.save(sl);
        return new ResponseEntity(
                new ApiResponse(true, "Data id : " + id + "deleted successfully"),
                HttpStatus.OK
        );
    }
}
