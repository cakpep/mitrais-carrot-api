package com.mitrais.carrot.controllers;

import com.mitrais.carrot.config.Config;
import com.mitrais.carrot.models.BazaarItem;
import com.mitrais.carrot.payload.ApiResponse;
import com.mitrais.carrot.repositories.BazaarItemRepository;
import com.mitrais.carrot.validation.exception.ResourceNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(Config.BASE_URI_API)
public class BazaarItemController {

    @Autowired
    public BazaarItemRepository bazaarItemsRepository;

    /**
     * get all data
     *
     * @return
     */
    @GetMapping("/bazaars-items")
    @ResponseBody
    public Iterable<BazaarItem> all() {
        return bazaarItemsRepository.findAllBydeletedIsNull();
    }

    /**
     * create new data
     *
     * @param body
     * @return
     */
    @PostMapping("/bazaars-items")
    @ResponseBody
    public BazaarItem save(@Valid @RequestBody BazaarItem body) {
        return bazaarItemsRepository.save(body);
    }

    /**
     * get detail by id
     *
     * @param id
     * @return
     */
    @GetMapping("/bazaars-items/{id}")
    @ResponseBody
    public BazaarItem detail(@PathVariable Integer id) {
        return bazaarItemsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
    }

    /**
     * update data
     *
     * @param id
     * @param body
     * @return
     */
    @PutMapping("/bazaars-items/{id}")
    @ResponseBody
    public BazaarItem update(@PathVariable Integer id, @Valid @RequestBody BazaarItem body) {
        BazaarItem model = bazaarItemsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        BeanUtils.copyProperties(body, model);
        model.setId(id);
        return bazaarItemsRepository.save(model);
    }

    /**
     * action delete to set is deleted = 1
     *
     * @param id
     * @return
     */
    @DeleteMapping("/bazaars-items/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        BazaarItem sl = bazaarItemsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        sl.setDeleted(true);
        bazaarItemsRepository.save(sl);
        return new ResponseEntity(
                new ApiResponse(true, "Data id : " + id + "deleted successfully"),
                HttpStatus.OK
        );
    }
}
