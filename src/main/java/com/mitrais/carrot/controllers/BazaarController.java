package com.mitrais.carrot.controllers;

import com.mitrais.carrot.config.Config;
import com.mitrais.carrot.models.Bazaar;
import com.mitrais.carrot.payload.ApiResponse;
import com.mitrais.carrot.repositories.BazaarRepository;
import com.mitrais.carrot.validation.exception.ResourceNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(Config.BASE_URI_API)
public class BazaarController {

    @Autowired
    BazaarRepository bazaarRepository;

    /**
     * get all data
     *
     * @return
     */
    @GetMapping("/bazaars")
    public Iterable<Bazaar> all() {
        return bazaarRepository.findAllBydeletedIsNull();
    }

    /**
     * create new data
     *
     * @param body
     * @return
     */
    @PostMapping("/bazaars")
    public Bazaar save(@Valid @RequestBody Bazaar body) {
        return bazaarRepository.save(body);
    }

    /**
     * get detail by id
     *
     * @param id
     * @return
     */
    @GetMapping("/bazaars/{id}")
    public Bazaar show(@PathVariable Integer id) {
        return bazaarRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
    }

    /**
     * update data
     *
     * @param id
     * @param body
     * @return
     */
    @PutMapping("/bazaars/{id}")
    public Bazaar update(@PathVariable Integer id, @Valid @RequestBody Bazaar body) {
        Bazaar model = bazaarRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        BeanUtils.copyProperties(body, model);
        model.setId(id);
        return bazaarRepository.save(model);
    }

    /**
     * action delete to set is deleted = 1
     *
     * @param id
     * @return
     */
    @DeleteMapping("/bazaars/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        Bazaar sl = bazaarRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Data", "id", id));
        sl.setDeleted(true);
        bazaarRepository.save(sl);
//        bazaarRepository.delete(sl.get());
        return new ResponseEntity(
                new ApiResponse(true, "Data id : " + id + "deleted successfully"),
                HttpStatus.OK
        );
    }
}
