package com.mitrais.carrot.models;

public enum RoleEnum {
    ROLE_STAFF,
    ROLE_ADMIN,
    ROLE_UNKNOWN,
    ROLE_MANAGER,
    ROLE_EMPLOYEE,
    ROLE_ROOT_ADMIN,
    ROLE_STAKEHOLDER,
    ROLE_SENIOR_MANAGER
}
