package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.BazaarItem;
import org.springframework.data.repository.CrudRepository;

/**
 * bazaar item repository
 */
public interface BazaarItemRepository extends CrudRepository<BazaarItem, Integer> {

    /**
     * find all data by is deleted value condition
     *
     * @param isdeleted
     * @return
     */
    public Iterable<BazaarItem> findBydeletedIn(Integer isdeleted);

    /**
     * find all data by is deleted condition
     * @return 
     */
    public Iterable<BazaarItem> findAllBydeletedIsNull();
}
