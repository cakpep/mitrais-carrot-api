package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.SharingLevel;
import org.springframework.data.repository.CrudRepository;

public interface SharingLevelRepository extends CrudRepository<SharingLevel, Integer> {

    /**
     * delete data
     *
     * @param deleted
     */
    @Override
    public void delete(SharingLevel deleted);

    @Override
    public Iterable<SharingLevel> findAll();

    /**
     * find all data by is deleted condition
     *
     * @param isdeleted
     * @return
     */
    public Iterable<SharingLevel> findBydeletedIn(Integer isdeleted);

    /**
     * find all data by is deleted condition
     *
     * @return
     */
    public Iterable<SharingLevel> findAllBydeletedIsNull();

}
