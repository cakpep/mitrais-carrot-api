package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.Role;
import com.mitrais.carrot.models.RoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("roleRepository")
public interface RoleRepository extends JpaRepository<Role, Integer> {

    /**
     * find by role name
     *
     * @param roleEnum
     * @return
     */
    public Role findByRoleName(RoleEnum roleEnum);

    /**
     * find all data by is deleted value condition
     *
     * @param isdeleted
     * @return
     */
    public Iterable<Role> findBydeletedIn(Integer isdeleted);

    /**
     * find all data by is deleted condition
     *
     * @return
     */
    public Iterable<Role> findAllBydeletedIsNull();
}
