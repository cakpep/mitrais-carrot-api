package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.Transactions;
import org.springframework.data.repository.CrudRepository;

public interface TransactionsRepository extends CrudRepository<Transactions, Integer> {

    /**
     * find all data by is deleted value condition
     *
     * @param isdeleted | deleted value
     * @return
     */
    public Iterable<Transactions> findBydeletedIn(Integer isdeleted);

    /**
     * find all data by is deleted condition
     *
     * @return
     */
    public Iterable<Transactions> findAllBydeletedIsNull();
}
