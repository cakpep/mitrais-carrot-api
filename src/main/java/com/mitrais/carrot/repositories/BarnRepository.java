package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.Barn;
import org.springframework.data.repository.CrudRepository;

/**
 * barn repository
 */
public interface BarnRepository extends CrudRepository<Barn, Integer> {

    /**
     * find all data by is deleted value condition
     * @param isdeleted
     * @return 
     */
    public Iterable<Barn> findBydeletedIn(Integer isdeleted);

    /**
     * find all data by is deleted condition
     * @return 
     */
    public Iterable<Barn> findAllBydeletedIsNull();
}
