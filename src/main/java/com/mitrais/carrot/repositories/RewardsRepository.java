package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.Rewards;
import org.springframework.data.repository.CrudRepository;

public interface RewardsRepository extends CrudRepository<Rewards, Integer> {

    /**
     * find all data by is deleted value condition
     *
     * @param isdeleted
     * @return
     */
    public Iterable<Rewards> findBydeletedIn(Integer isdeleted);

    /**
     * find all data by is deleted condition
     *
     * @return
     */
    public Iterable<Rewards> findAllBydeletedIsNull();
}
