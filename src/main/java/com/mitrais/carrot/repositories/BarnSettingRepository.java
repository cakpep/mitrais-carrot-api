package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.BarnSetting;
import org.springframework.data.repository.CrudRepository;

/**
 * barn setting repository
 */
public interface BarnSettingRepository extends CrudRepository<BarnSetting, Integer> {

    /**
     * find all data by is deleted value condition
     *
     * @param isdeleted
     * @return
     */
    public Iterable<BarnSetting> findBydeletedIn(Integer isdeleted);

    /**
     * find all data by is deleted condition
     * @return 
     */
    public Iterable<BarnSetting> findAllBydeletedIsNull();
    
}
