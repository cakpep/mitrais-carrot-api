package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * find user by email
     *
     * @param email
     * @return
     */
    public User findByEmail(String email);

    /**
     * find by username or email
     *
     * @param username
     * @param email
     * @return
     */
    public Optional<User> findByUserNameOrEmail(String username, String email);

    /**
     * find user by user id list
     *
     * @param userIds
     * @return
     */
    public List<User> findByIdIn(List<Long> userIds);

    /**
     * find user by username
     *
     * @param userName
     * @return
     */
    public User findByUserName(String userName);

    /**
     * check user exist or not by username
     *
     * @param username
     * @return
     */
    public Boolean existsByUserName(String username);

    /**
     * check user exist or not by email
     *
     * @param email
     * @return
     */
    public Boolean existsByEmail(String email);

    /**
     * find all data by is deleted value condition
     *
     * @param isdeleted
     * @return
     */
    public Iterable<User> findBydeletedIn(Integer isdeleted);

    /**
     * find all data by is deleted condition
     *
     * @return
     */
    public Iterable<User> findAllBydeletedIsNull();
}
