package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.Bazaar;
import org.springframework.data.repository.CrudRepository;

/**
 * bazaar repository
 */
public interface BazaarRepository extends CrudRepository<Bazaar, Integer> {

    /**
     * find all data by is deleted value condition
     *
     * @param isdeleted
     * @return
     */
    public Iterable<Bazaar> findBydeletedIn(Integer isdeleted);

    /**
     * find all data by is deleted condition
     *
     * @return
     */
    public Iterable<Bazaar> findAllBydeletedIsNull();
}
