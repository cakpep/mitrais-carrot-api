package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.ShareType;
import org.springframework.data.repository.CrudRepository;

public interface ShareTypeRepository extends CrudRepository<ShareType, Integer> {

    /**
     * find all data by is deleted value condition
     *
     * @param isdeleted
     * @return
     */
    public Iterable<ShareType> findBydeletedIn(Integer isdeleted);

    /**
     * find all data by is deleted condition
     *
     * @return
     */
    public Iterable<ShareType> findAllBydeletedIsNull();
}
